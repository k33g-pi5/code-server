# Code server template

## First

### SSH Key (GitLab.com)

```bash
# for example
ssh-keygen -t ed25519 -C "K33G_PI5_COLOSSUS_CODE_SERVER"
cat /home/k33g/.ssh/id_ed25519.pub
# register the publik key on GitLab.com
```


## How to

```bash
git clone https://gitlab.com/k33g-pi5/code-server.git
cd code-server
docker compose build # if you change something into docker-dev-environment/Dockerfile
docker compose up
```

## Update `compose.yaml`

- `entrypoint`: HTTP port of Code Server and bind address
- `build.args`: variables used by the docker build
- `ports`: set the HTTP port of Code Server (and the other used port)

## Code server configuration

Set password and TLS certificate+key: Update this file: `workspace/.config/code-server`

## Start something at boot (faking k33g user)

```bash
sudo nano /etc/rc.local
# add
cd /home/k33g/code-server
su k33g -c 'docker compose up'
# do not remove `exit 0` at the end
sudo chmod +x /etc/rc.local
```
