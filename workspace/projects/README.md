# Projects

## Open a project with Coder Server: 

- http://0.0.0.0:3000/?folder=/home/coder/projects/project_name
- With a domain name + certificate:
  - https://<your.domain.name>:3000/?folder=/home/coder/projects/project_name
  - ex: https://code-server.bots.garden:3000/?folder=/home/coder/projects/demo


## SSH Key (GitLab.com)

```bash
ssh-keygen -t ed25519 -C "K33G_PI5_COLOSSUS"
cat /home/code-server/.ssh/id_ed25519.pub
# register the publik key on GitLab.com
```

## Git config

```bash
git config --global user.name "Bob Morane"
git config --global user.email "bob@bots.garden"
```

## Put the project under GitLab

```bash
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.com:k33g-pi5/code-server.git
git add .
git commit -m "🎉 Initial commit"
git push --set-upstream origin main
```
