# Certificates

## Let's Encrypt certificate

```bash
brew install certbot # on MacOS

sudo certbot certonly \
--server https://acme-v02.api.letsencrypt.org/directory \
--manual -d '*.bots.garden'
# where *.bots.garden is your domain name
# then deploy a DNS TXT record under the name:
# _acme-challenge.bots.garden.
# with the provided value

sudo cp /etc/letsencrypt/live/bots.garden/fullchain.pem ./_.bots.garden.crt
sudo cp /etc/letsencrypt/live/bots.garden/privkey.pem ./_.bots.garden.key
sudo chmod 777 _.bots.garden.*

cp _.bots.garden.crt code-server.bots.garden.crt
cp _.bots.garden.key code-server.bots.garden.key
```

> Copy certificates to remote host
```bash
scp colossus.bots.garden.* k33g@colossus.local:code-server/workspace/certs
```


## Self signed certificate

> TODO













Add `0.0.0.0 coder.bots.garden` to the `hosts` file of the client machine

